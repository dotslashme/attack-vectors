package main

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis/v8"
	"log"
)

var (
	redisClient = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
)

func Persist(data map[string][]string) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	for k, v := range data {
		data, err := json.Marshal(v)
		if err != nil {
			log.Fatalln(err)
		}
		err = redisClient.Set(ctx, k, data, -1).Err()
		if err != nil {
			log.Fatalln(err)
		}
	}
}
