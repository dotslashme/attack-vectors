# Deep web attack vector

The deep web is a name for all web resources that are not indexed by search engines, making them deep or only reachable through hyper-links. A common scenario is to hide resources from search engines that should not be available to the public through the use of a robots.txt file. This software fetches that file and exposes the deep web resources.

It would be a mistake to see all of these resources as vulnerabilities, rather they should be viewed as resources of interest.