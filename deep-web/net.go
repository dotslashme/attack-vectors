package main

import (
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/apparentlymart/go-cidr/cidr"
)

type Scan struct {
	id      int
	address string
}

type Result struct {
	scan Scan
	data string
}

const (
	workers = 900
)

var (
	httpClient = &http.Client{
		Timeout: 3 * time.Second,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	scans   = make(chan Scan, 10)
	results = make(chan Result, 10)
	done    = make(chan bool)

	dbData = make(map[string][]string)
)

func ScanNet(netCidr string) {
	startTime := time.Now()

	_, network, err := net.ParseCIDR(netCidr)
	if err != nil {
		return
	}

	go allocate(network)
	go result(done)
	createWorkerPool()
	<-done

	Persist(dbData)
	endTime := time.Now()
	log.Printf("Operation took %.2f seconds\n", endTime.Sub(startTime).Seconds())
}

func allocate(network *net.IPNet) {
	startAddr, endAddr := cidr.AddressRange(network)
	var i int

	for startAddr.String() != endAddr.String() {
		scan := Scan{i, startAddr.String()}
		startAddr = cidr.Inc(startAddr)
		i++
		scans <- scan
	}

	close(scans)
}

func createWorkerPool() {
	var wg sync.WaitGroup

	for i := 0; i < workers; i++ {
		wg.Add(1)
		go doWork(&wg)
	}

	wg.Wait()
	close(results)
}

func doWork(wg *sync.WaitGroup) {
	for scan := range scans {
		results <- Result{scan, scanHost(scan.address)}
	}

	wg.Done()
}

func result(done chan bool) {
	for result := range results {
		rows := strings.Split(result.data, "\n")
		var disallowed []string
		for _, row := range rows {
			if strings.Contains(row, "Disallow:") {
				r := strings.Fields(row)
				if len(r) != 2 {
					continue
				} else {
					disallowed = append(disallowed, r[1])
				}
			}
		}

		dbData[result.scan.address] = disallowed
	}
	done <- true
}

func scanHost(ip string) string {
	var response string

	req, err := http.NewRequest("GET", "http://"+ip+"/robots.txt", nil)
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Add("Accept", "text/plain")

	res, err := httpClient.Do(req)
	if err != nil {
		return ""
	}
	defer res.Body.Close()

	if res.StatusCode == http.StatusOK {
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return ""
		}
		response = string(body)
	}
	return response
}
